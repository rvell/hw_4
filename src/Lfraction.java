import java.util.*;

import static java.lang.Math.toIntExact;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {

       // valueOf("1!2");
       // valueOf("1/");
        System.out.println(valueOf("3/-x"));
       /*
       Lfraction f1 = new Lfraction(2,5);
       Lfraction f2 = new Lfraction(1,0);
       f1.divideBy(f2);
       valueOf(1/);
       */
   }
   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
       if(b == 0){
           throw new RuntimeException("Denominator can not be zero:-> " + b);
       }
      numerator = a;
      denominator = b;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return "" + Long.toString(this.numerator) + "/" + Long.toString(this.denominator);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      Lfraction another = (Lfraction) m;
      if (this.denominator == another.denominator){
        return this.numerator == another.numerator;
      }
      else {
          int dif;
          if(this.denominator > another.denominator){
              dif = toIntExact(this.denominator/another.denominator);
          }
          else {
              dif = toIntExact(another.denominator/this.denominator);
          }
       return (this.numerator*dif) == another.numerator;
      }
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.numerator, this.denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
       if(this.denominator == m.denominator){
           Lfraction another = new Lfraction(this.numerator + m.numerator, this.denominator);
            return another;
       }
       else {
           Lfraction another = new Lfraction((this.numerator * m.denominator)+
                   (m.numerator * this.denominator),this.denominator*m.denominator);
           return another;
       }
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
       Lfraction another = new Lfraction(this.numerator * m.numerator, this.denominator * m.denominator).reduce();
      return another;
   }

   private Lfraction reduce(){
       long common;
       if(this.numerator == this.denominator){
           common = this.numerator;
       }
       else {
           long denominator1;
           long denominator2;
           if(this.numerator > this.denominator){
               denominator1 = this.numerator;
               denominator2 = this.denominator;
           }
           else{
               denominator1 = this.denominator;
               denominator2 = this.numerator;
           }

           long a;
           while(denominator2 != 0){
               a = denominator2;
               denominator2 = denominator1 % denominator2;
               denominator1 = a;
           }
           common = denominator1;
       }
       if (denominator < 0){
           this.numerator = this.numerator * -1;
           this.denominator = this.denominator * -1;
       }

       return new Lfraction(this.numerator / common, this.denominator /common);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
       if (this.numerator == 0){
           throw new RuntimeException("Denominator can not be 0: "
                   + this.numerator + "/" + this.denominator);
       }
       else if(this.numerator < 0){
           return new Lfraction(-1*(this.denominator),-1*this.numerator);
       }
      return new Lfraction(this.denominator, this.numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-1*this.numerator,this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
       if (this.denominator == m.denominator){
           Lfraction another = new Lfraction(this.numerator - m.numerator, this.denominator);
           return another;
       }
       else {
           Lfraction another = new Lfraction ((this.numerator * m.denominator) -
                   (m.numerator * this.denominator),this.denominator * m.denominator);
           return another;
       }
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
       if(m.numerator == 0 || m.denominator == 0){
           throw new RuntimeException("Impossible to divide with: " + m.numerator + "/" + m.denominator);
       }
       Lfraction another = new Lfraction(Math.abs(this.numerator * m.denominator), Math.abs(this.denominator * m.numerator));
      return another;
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
       if(this.equals(m)){
           return 0;
       }
       else if(this.numerator * m.denominator < m.numerator * this.denominator){
           return -1;
       }
       else return 1;
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return Math.round(this.numerator / this.denominator);
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
    if(this.numerator >= this.denominator || -this.numerator >= this.denominator){
        return new Lfraction(this.numerator % this.denominator, this.denominator);
    }else
        return new Lfraction(this.numerator, this.denominator);

   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {

       return (double)this.numerator / this.denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
       return new Lfraction(Math.round(f*d) ,d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {

       // based on second feeback round on 06/11
       if(s.length() < 3){
           throw new RuntimeException("Not enough characters in string. Must be in the shape of X/X. Yours: " + s);
       }

       int counter = 0;
       for(int i = 0; i <s.length(); i++){
           if(s.charAt(i)== '/'){
               counter++;
               if(counter > 1){
                   throw new RuntimeException("Too many divisions." + s + " Input has to be in the shape of ..X/X..");
               }
           }
       }

       String[] halved = s.split("/");
       String num;
       String den;


       try {
           num = interpret(halved[0]);
           den = interpret(halved[1]);
       }
       catch(RuntimeException e){
           throw new RuntimeException("Invalid String: " + s + "  - Must be in the shape of ...X/X... .");
       }

       try {
            return new Lfraction(Long.parseLong(num), Long.parseLong(den)).reduce();
       }
       catch(NumberFormatException e){
           throw new RuntimeException("Input string must contain / ." + s);
       }

   }

   private static String interpret (String s) {
       if (s.length() == 0) {
           throw new RuntimeException("Insufficient members in" + s);
       }
       for (int i = 0; i < s.length(); i++) {
           char c = s.charAt(i);
           char nc = ' ';
           if (i + 1 < s.length()) {
               nc = s.charAt(i + 1);
           }

           if((c == '-' & nc >= 0 & nc <= '9') | (c >= '0' & c <= '9')){
               StringBuilder buffer = new StringBuilder();
               buffer.append(c);
               i++;
               for(; i < s.length(); i++){
                   c = s.charAt(i);
                   if(c >= '0' & c <= '9') {
                       buffer.append(c);
                   }else{
                       break;
                   }
               }
               return buffer.toString();
           }
       }
       throw new RuntimeException("Invalid string! " + s + " Contains symbols other than numbers!");
   }
}

